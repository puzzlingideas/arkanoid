/*
 * This is the entrance point of your game.
 * When the document is finished loading the following function will be executed.
 */
function main() {
	
	var CANVAS_WIDTH = 224,
		CANVAS_HEIGHT = 256;

	var layer = M.createLayer();

	var background = new M.Entity();
	background.shows("back").as("rectangle").setWidth(CANVAS_WIDTH).setHeight(CANVAS_HEIGHT).setLeft(0).setTop(0).setColor("black");

	var helloWorld = new M.Entity();

	helloWorld.has("direction").set(1, 1);

	helloWorld.shows("messageBackground").as("rectangle").set({
		x: 100, y: 100, width: 60, fillStyle: "red", height: 60
	});
	helloWorld.shows("message").as("text").set({
		x: 100, y: 100, size: 20, fillStyle: "white", text: "Hello World!"
	});

	helloWorld.does("bounce", function(entity, attributes, views) {
		views.eachValue(function (view) {

			var direction = attributes.get("direction");
			view.offset(direction.x, direction.y);
		
			if ( view.getRight() > CANVAS_WIDTH || view.getLeft() < 0 ) {
				direction.x *= -1;
			}
		
			if ( view.getBottom() > CANVAS_HEIGHT || view.getTop() < 0 ) {
				direction.y *= -1;
			}

		});
	});

	helloWorld.does("spinAround", function(entity, attributes, views) {
		views.get("messageBackground").offsetRotation(0.01);
	});

	layer.push(background);
	layer.push(helloWorld);

}