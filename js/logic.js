function ballCollisionHandler (manifold) {
	var self = manifold.entity,
		direction = self.attributes.get("direction"),
		location = self.attributes.get("location"),
		collidedWith = manifold.collidedWith;

	if ( manifold.collisionInX ) {
		direction.set(-direction.x, direction.y);
	} else if ( manifold.collisionInY ) {
		direction.set(direction.x, -direction.y);
	}

	if (collidedWith.TYPE == "block" ) {
		M.remove(collidedWith).from("blocks");
	} else if (collidedWith.TYPE == "pad") {

		//distance from pad center
		var d = getXSpeedFromCollision(manifold);
		direction.setX(d);

	}
}
function getXSpeedFromCollision(manifold) {
	var d = ((manifold.viewFromSelf.getX() - manifold.viewFromOther.getX()) / 
		manifold.viewFromOther._halfWidth)*2.5; //divisor
	if (Math.abs(d)<0.5 && d > 0) d=0.5;
	if (Math.abs(d)<0.5 && d < 0) d=-0.5;
	return d;
}