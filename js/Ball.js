M.registerEntity("ball", function () {
	var ball = new M.Entity();
	
	ball.has("preventMoveOnCollision", true);
	ball.has("areaToStayIn", M.game.config.areaToStayIn);
	ball.has("direction").set(1,-1);
	ball.has("speed", 1);
	ball.has("location").set(M.game.config.CANVAS_WIDTH / 2, M.game.config.CANVAS_HEIGHT - 25);
	ball.has("rotation", 0);

	ball.shows("body").as("rectangle").set({
		width: 5, height: 5, color: "red", x: 0, y: 0
	});

	ball.does("collide");
	ball.does("moveWithSpeedAndDirection");
	ball.does("fixViewsToEntity");
	
	ball.addEventListener("onCollision", ballCollisionHandler);

	return ball;
});